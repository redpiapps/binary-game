package redpi.apps.binarygame;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;

import java.util.Random;

import redpi.apps.utils.MyConstants;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PlayFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PlayFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlayFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private int MODE ;
    private static final int EASY = 1 ;
    private static final int MEDIUM = 2;
    private InterstitialAd interstitial;
    AdRequest adRequest;

    private static final int REQUEST_LEADERBOARD = 17;
    private static final int REQUEST_ACHIEVEMENTS = 18;


    TextView steps;

    boolean leaderClicked;
    boolean achClicked;
    int stepsVal;
    TextView[] tileArray;
    int totalCount;
    Random random;
    public int width;
    int padding_px;
    int tileWidth;
    public int height;
    SharedPreferences pref;
    private GoogleApiClient mGoogleApiClient;
    RelativeLayout helpContainer;
    Typeface tf3;
    boolean helpInited;
    private boolean hard;
    TextView modeVal;
    LinearLayout tileContainer;
    private int SIZE;
    private int TILE_COUNT;
    private FragmentManager fragmentManager;
    private RelativeLayout gameContainer;
    boolean winShowed = false;

    private RelativeLayout fragmentContainer;

    private MenuLayout menuLayout;
    private LevelFragment levelFragment;

    private int totalTileValue=0;

    private int greenCount = 0;

    private static final int[] SHUFFLE_LEVEL1 = {4,7,12,15,4,7,4,7,7,7,7,7};
    private static final int[] SHUFFLE_LEVEL2 = {0,1,2,3,4,5,6,7,8,9,10,11};
    private static final int[] SHUFFLE_LEVEL3 = {0,1,2,3,4,20,21,22,23,24,12,17};
    private static final int[] SHUFFLE_LEVEL4 = {0,1,2,3,4,5,30,31,32,33,34,35};

    private Context context;

    private int selectedLevel=0;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View rootView;

    private ImageButton refresh;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;

    public static PlayFragment newInstance(int size, int mode, int selectedLevel) {
        PlayFragment fragment = new PlayFragment();
        Bundle args = new Bundle();
        args.putInt("SIZE",size);
        args.putInt("MODE", mode);
        args.putInt("LEVEL",selectedLevel);
        fragment.setArguments(args);
        return fragment;
    }

    public PlayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            MODE = getArguments().getInt("MODE");
            SIZE = getArguments().getInt("SIZE");
            selectedLevel = getArguments().getInt("LEVEL");

            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                builder = new AlertDialog.Builder(getActivity());
            } else {
                builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
            }
            builder.setTitle(getString(R.string.restart));
            builder.setMessage(getString(R.string.are_you_sure_restart));
            builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    refresh();
                    dialog.cancel();
                }
            });
            dialog = builder.create();
        }
    }

    public void refresh(){
        winShowed=false;
        populateManual(SIZE);
        shuffle();
    }

    public void init() {


        context = getActivity();
        totalCount = 0;
        helpInited = false;
        leaderClicked = false;
        achClicked = false;

        Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
                MyConstants.FONT_EPYVAL);
        tf3 = Typeface.createFromAsset(context.getAssets(), MyConstants.FONT_QUATR);

        tileContainer = (LinearLayout) rootView.findViewById(R.id.tile_container);
        fragmentContainer = (RelativeLayout)rootView.findViewById(R.id.fragmentContainer);
        gameContainer = (RelativeLayout)rootView.findViewById(R.id.gameContainer);
        random = new Random();
        random.setSeed(random.nextInt());

		/*leader = (ImageButton) findViewById(R.id.leader);
		leader.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				leaderClicked = true;
				if (!mGoogleApiClient.isConnected())
					mGoogleApiClient.connect();
				else
					startActivityForResult(Games.Leaderboards
							.getLeaderboardIntent(mGoogleApiClient,
									MyConstants.LEADERBOARD),
							REQUEST_LEADERBOARD);
			}
		});
		restart = (ImageButton) findViewById(R.id.restart);
		restart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				populate(hard);
			}
		});

		win = (TextView) findViewById(R.id.win);
		win.setTypeface(tf3);
		gameEndBg = (RelativeLayout) findViewById(R.id.gameEndBg);
		gameEndBg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
*/

        ((TextView) rootView.findViewById(R.id.stepsTitle)).setTypeface(tf2);
        ((TextView) rootView.findViewById(R.id.modeTitle)).setTypeface(tf2);
        ((TextView) rootView.findViewById(R.id.mode_val)).setTypeface(tf2);

        refresh = (ImageButton)rootView.findViewById(R.id.refresh_game);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });




        steps = (TextView) rootView.findViewById(R.id.steps_val);
        steps.setTypeface(tf2);
        stepsVal = 0;

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        //padding_px = (int) convertDpToPixel(MyConstants.padding_dp, this);
        padding_px = (int)getResources().getDimension(R.dimen.margin_tile);
        tileWidth = (width - MyConstants.MARGIN_COUNT * padding_px) / 4;

        pref = context.getSharedPreferences(MyConstants.PREF_NAME, Context.MODE_PRIVATE);


        helpContainer = (RelativeLayout) rootView.findViewById(R.id.help_container);

        if (pref.getBoolean(MyConstants.SHOW_HELP, true)) {
            initHelp();
            SharedPreferences.Editor edit = pref.edit();
            edit.putBoolean(MyConstants.SHOW_HELP, false);
            edit.apply();
            helpContainer.setVisibility(View.VISIBLE);

        }

        modeVal=(TextView) rootView.findViewById(R.id.mode_val);
        modeVal.setText(""+selectedLevel);

        helpContainer.getLayoutParams().height = height;
        helpContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                helpContainer.setVisibility(View.GONE);
            }
        });

       /* mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Games.API)
                .addScope(Games.SCOPE_GAMES)
                        // add other APIs and scopes here as needed
                .build();*/

        interstitial = new InterstitialAd(context);
        interstitial.setAdUnitId(MyConstants.INTER_AD_ID);
        adRequest = new AdRequest.Builder().addTestDevice(
                "9A01AB5160C9283CA9FA132A6E12A57F").build();

        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // TODO Auto-generated method stub
                super.onAdClosed();
                adRequest = new AdRequest.Builder().addTestDevice(
                        "9A01AB5160C9283CA9FA132A6E12A57F").build();
                interstitial.loadAd(adRequest);

            }



        });
        interstitial.loadAd(adRequest);

        initScoreLayout();
    }

    public void clickHandler(View view)
    {
        int tile1Pos = (Integer) view.getTag();

        int tile2Pos = tile1Pos + SIZE;
        int tile3Pos = tile1Pos - SIZE;
        int tile4Pos = tile1Pos - 1;
        int tile5Pos = tile1Pos + 1;

        animateFunc(view);
        if (tile2Pos < TILE_COUNT)
            animateFunc(tileArray[tile2Pos]);
        if (tile3Pos > -1) {
            animateFunc(tileArray[tile3Pos]);

        }

        if (tile1Pos % SIZE != 0)
            animateFunc(tileArray[tile4Pos]);
        if ((tile1Pos + 1) % SIZE != 0)
            animateFunc(tileArray[tile5Pos]);

    }
    public void clickHandler2(View view)
    {
        int tile1Pos = (Integer) view.getTag();

        int tile2Pos = tile1Pos + SIZE;
        int tile3Pos = tile1Pos - SIZE;
        int tile4Pos = tile1Pos - 1;
        int tile5Pos = tile1Pos + 1;

        animateFunc2(view);
        if (tile2Pos < TILE_COUNT)
            animateFunc2(tileArray[tile2Pos]);
        if (tile3Pos > -1) {
            animateFunc2(tileArray[tile3Pos]);

        }

        if (tile1Pos%SIZE !=0)
            animateFunc2(tileArray[tile4Pos]);
        if ((tile1Pos+1)%SIZE != 0)
            animateFunc2(tileArray[tile5Pos]);

    }
    @Override
    public void onClick(View view) {

        stepsVal += 1;
        steps.setText("" + stepsVal);
        clickHandler(view);
		/*
		 * animateFunc(view, tileArray[tag + 4], tileArray[tag - 4],
		 * tileArray[tag - 1], tileArray[tag + 1]);
		 */

        //check();

    }

    public void check() {

        if(totalCount ==0 || totalCount==totalTileValue ){
            if(!winShowed)
                showWin();
            return;
        }
        if((MODE == MEDIUM) && (greenCount == 0) && (totalCount==TILE_COUNT)){
            if(!winShowed)
                showWin();
        }
    }

    public void showWin(){
        winShowed = true;
        //mListener.callUpdateGameServices();
        LevelCompleteDialog completeDialog = LevelCompleteDialog.newInstance(selectedLevel,stepsVal);
        fragmentManager = getChildFragmentManager();
        completeDialog.show(fragmentManager,"dialog");

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                });
            }
        }

        return animation;
    }

    public void animateFunc(final View view) {


        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "RotationY", 0,
                90);
        animator.setDuration(MyConstants.NORMAL_DURATION);
        animator.setRepeatCount(1);
        animator.setRepeatMode(Animation.REVERSE);
        animator.addListener(new Animator.AnimatorListener() {

            TextView tempView = (TextView) view;
            int textVal;

            @Override
            public void onAnimationStart(Animator arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animator arg0) {
                // TODO Auto-generated method stub
                textVal = Integer.parseInt(tempView.getText().toString());
                if (textVal == 0) {
                    totalCount += 1;
                    tempView.setText("1");
                    tempView.setBackgroundColor(MyConstants.OneColor);

                } else if (textVal == 1) {
                    if(MODE == EASY) {
                        totalCount -= 1;
                        tempView.setText("0");
                        tempView.setBackgroundColor(MyConstants.zeroColor);
                    }
                    else
                    {
                        totalCount += 1;
                        tempView.setText("2");
                        greenCount++;
                        tempView.setBackgroundColor(MyConstants.twoColor);
                    }

                }
                else if(textVal == 2)
                {
                    if(MODE == MEDIUM)
                    {
                        totalCount -=2;
                        tempView.setText("0");
                        greenCount--;
                        tempView.setBackgroundColor(MyConstants.zeroColor);
                    }

                }

            }

            @Override
            public void onAnimationEnd(Animator arg0) {
                // TODO Auto-generated method stub
                check();

            }

            @Override
            public void onAnimationCancel(Animator arg0) {
                // TODO Auto-generated method stub

            }
        });
        animator.start();

    }
    public void animateFunc2(final View view) {

        TextView tempView = (TextView) view;
        int textVal;
        textVal = Integer.parseInt(tempView.getText().toString());
        if (textVal == 0) {
            totalCount += 1;
            tempView.setText("1");
            tempView.setBackgroundColor(MyConstants.OneColor);

        } else if(textVal==1){
            if(MODE == EASY) {
                totalCount -= 1;
                tempView.setText("0");
                tempView.setBackgroundColor(MyConstants.zeroColor);
            }else{
                totalCount +=1;
                tempView.setText("2");
                greenCount++;
                tempView.setBackgroundColor(MyConstants.twoColor);
            }
        }else if(textVal == 2){
            totalCount-=2;
            tempView.setText("0");
            greenCount--;
            tempView.setBackgroundColor(MyConstants.zeroColor);
        }

    }


    public void initHelp() {

        helpContainer.setPadding(-padding_px, -padding_px, -padding_px,
                -padding_px);
        ImageView helpHand = (ImageView) rootView.findViewById(R.id.help_hand);
        TextView helpInfo = (TextView) rootView.findViewById(R.id.help_info);
        helpInfo.setTypeface(tf3);
        helpHand.setX(1.5f * tileWidth + 5 * padding_px);
        helpHand.setY(1.5f * tileWidth + 5 * padding_px);
        helpInfo.setY(helpHand.getY() + tileWidth + padding_px);
        ImageView win1 = (ImageView) rootView.findViewById(R.id.win1);
        ImageView win2 = (ImageView) rootView.findViewById(R.id.win2);

        win1.setY(helpInfo.getY() + tileWidth + 5 * padding_px);
        win1.setX(tileWidth / 2 + padding_px);
        win2.setX(2.5f * tileWidth + 3 * padding_px);

        win2.setY(helpInfo.getY() + tileWidth + 5 * padding_px);

        int winDimen = tileWidth + 4 * padding_px;
        win1.getLayoutParams().height = winDimen;
        win1.getLayoutParams().width = winDimen;
        win2.getLayoutParams().height = winDimen;
        win2.getLayoutParams().width = winDimen;

        TextView winInfo = (TextView) rootView.findViewById(R.id.win_information);
        winInfo.setTypeface(tf3);
        winInfo.setY(win1.getY() + winDimen + 2 * padding_px);

        helpInited = true;

    }

    private void initScoreLayout() {
        RelativeLayout stepsContainer = (RelativeLayout) rootView.findViewById(R.id.steps_container);
        RelativeLayout bestContainer = (RelativeLayout) rootView.findViewById(R.id.best_container);
        RelativeLayout levelContainer = (RelativeLayout) rootView.findViewById(R.id.mode_container);

        int availableHeightForScore = (int) (height-width-getResources().getDimension(R.dimen.dp10));
        int widthToConsider = width - 80;



        stepsContainer.getLayoutParams().width = widthToConsider/3;
        stepsContainer.getLayoutParams().height = availableHeightForScore/2;
        bestContainer.getLayoutParams().width = widthToConsider/3;
        bestContainer.getLayoutParams().height = availableHeightForScore/2;
        refresh.getLayoutParams().width = widthToConsider/5;
        refresh.getLayoutParams().height = widthToConsider/5;
        levelContainer.getLayoutParams().width = widthToConsider/3;
        levelContainer.getLayoutParams().height=availableHeightForScore/2;
        ((RelativeLayout.LayoutParams)stepsContainer.getLayoutParams()).setMargins(20, 0, 20, 0);
        ((RelativeLayout.LayoutParams)levelContainer.getLayoutParams()).setMargins(20,0,20,0);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         rootView = inflater.inflate(R.layout.fragment_play, container, false);
        init();
        populateManual(SIZE);
        shuffle();
        initHelp();



        return rootView;
    }

    public void populateManual(int size)
    {
        totalCount = 0;
        greenCount = 0;
        helpInited = false;
        leaderClicked = false;
        achClicked = false;
        stepsVal = 0;
        steps.setText("0");
        TILE_COUNT = SIZE*SIZE;
        totalTileValue = TILE_COUNT*MODE;
        tileContainer.removeAllViews();
        int marginCount = size*2 + 2;
        tileArray = new TextView[size*size];
        int margin = (int)getResources().getDimension(R.dimen.margin_tile);
        int tileWidth = (width - marginCount*margin)/size;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(tileWidth,tileWidth);
        int fontSize = (int)getResources().getDimension(R.dimen.font_size);
        params.setMargins(margin,margin,margin,margin);
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                MyConstants.FONT_CCAP);



        int fillCount=0;
        for(int i = 0;i<size;++i)
        {
            LinearLayout row = new LinearLayout(context);
            for(int j=0;j<size;++j)
            {
                TextView textView = new TextView(context);
                textView.setGravity(Gravity.CENTER);
                textView.setLayoutParams(params);

                textView.setTypeface(tf);
                textView.setBackgroundColor(MyConstants.OneColor);
                row.addView(textView);
                textView.setText("1");
                textView.setTextSize(0);
                totalCount++;
                textView.setTag(fillCount);
                textView.setOnClickListener(this);
                tileArray[fillCount] = textView;
                fillCount++;
            }
            tileContainer.addView(row);
        }


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void callUpdateGameServices();
    }
    public void shuffle()
    {
        int tempVal;
        int[] shuffleArray =null ;
        if(selectedLevel == LevelFragment.LEVEL_1){
            shuffleArray = SHUFFLE_LEVEL1;
        }else if(selectedLevel == LevelFragment.LEVEL_2){
            shuffleArray = SHUFFLE_LEVEL2;

        }else if(selectedLevel == LevelFragment.LEVEL_3){
            shuffleArray = SHUFFLE_LEVEL3;

        }else if(selectedLevel == LevelFragment.LEVEL_4){
            shuffleArray = SHUFFLE_LEVEL4;

        }
        for(int i=0;i<12;++i)
        {
            clickHandler2(tileArray[shuffleArray[i]]);
        }

        if(selectedLevel == LevelFragment.LEVEL_4){
            clickHandler2(tileArray[14]);
            clickHandler2(tileArray[15]);
            clickHandler2(tileArray[20]);
            clickHandler2(tileArray[21]);



        }
    }

}
