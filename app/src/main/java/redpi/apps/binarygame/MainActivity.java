package redpi.apps.binarygame;


import redpi.apps.utils.MyConstants;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;

public class MainActivity extends FragmentActivity implements SplashFragment.OnFragmentInteractionListener,
		MenuLayout.OnFragmentInteractionListener,
		LevelFragment.OnFragmentInteractionListener,
		PlayFragment.OnFragmentInteractionListener,
		LevelCompleteDialog.OnFragmentInteractionListener,
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

    
	private static final int REQUEST_LEADERBOARD = 17;
	private static final int REQUEST_ACHIEVEMENTS = 18;
	private static final String TRANSACTION_MENU_TO_LEVEL = "menuToLevel";
	private static final String TRANSACTION_LEVEL_TO_PLAY = "levelToPlay";

	private int MODE ;
	private static final int EASY = 1 ;
	private static final int MEDIUM = 2;
	private int SIZE;



	boolean leaderClicked;
	boolean achClicked;


	private GoogleApiClient mGoogleApiClient;
    private FragmentManager fragmentManager;
	private MenuLayout menuLayout;
	private LevelFragment levelFragment;
	private PlayFragment playFragment;
	private int selectedLevel=0;
	private SharedPreferences pref;

	private AlertDialog dialog = null;
	private AlertDialog.Builder builder = null;

	@Override
	public void onBackPressed() {
		//super.onBackPressed();

			if(fragmentManager.getBackStackEntryCount()==0) {
				super.onBackPressed();
			}else {
				if(fragmentManager.getBackStackEntryCount()>1) {
					dialog.show();
				}else{
					fragmentManager.popBackStack();
				}

			}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        init();
        try{
            getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.bgColor)));
        }catch(Exception e){}
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);

        transaction.replace(R.id.fragmentContainer,new SplashFragment()).commit();
	
	}

  
	
	public void init() {
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			builder = new AlertDialog.Builder(this);
		} else {
			builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
		}
		builder.setTitle(getString(R.string.exit));
		builder.setMessage(getString(R.string.are_you_sure));
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				fragmentManager.popBackStack(TRANSACTION_LEVEL_TO_PLAY, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			}
		});
		dialog = builder.create();
		pref = getSharedPreferences(MyConstants.PREF_NAME, Context.MODE_PRIVATE);

		leaderClicked = false;
		achClicked = false;
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Games.API)
				.addScope(Games.SCOPE_GAMES)
				// add other APIs and scopes here as needed
				.build();

	}

  
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.refresh) {
			new AlertDialog.Builder(this)
					.setTitle("Reset Puzzle")
					.setMessage("Are you sure you want to reset the puzzle?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							}).show();
			// populate();
			return true;
		}  else if (id == R.id.leaderboard) {
			if (!mGoogleApiClient.isConnected())
				mGoogleApiClient.connect();
			else
				startActivityForResult(Games.Leaderboards.getLeaderboardIntent(
						mGoogleApiClient, MyConstants.LEADERBOARD),
						REQUEST_LEADERBOARD);
			leaderClicked = true;
		} else if (id == R.id.ach) {
			if (!mGoogleApiClient.isConnected())
				mGoogleApiClient.connect();
			else
				startActivityForResult(
						Games.Achievements
								.getAchievementsIntent(mGoogleApiClient),
						REQUEST_ACHIEVEMENTS);
			achClicked = true;
		} else if (id == R.id.rate) {
			Uri uri = Uri.parse("market://details?id="
					+ "redpi.apps.binarygame");
			Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
			try {
				startActivity(goToMarket);
			} catch (ActivityNotFoundException e) {
				startActivity(new Intent(
						Intent.ACTION_VIEW,
						Uri.parse("http://play.google.com/store/apps/details?id=redpi.apps.binarygame")));
			}
		} else if (id == R.id.share) {
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.setType("text/plain");

			//sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

			sendIntent.putExtra(Intent.EXTRA_TEXT,
					"Try out this puzzle : Binary Game\n"
							+ MyConstants.GAME_URL);
			startActivity(sendIntent);
		}
		return super.onOptionsItemSelected(item);
	}


	private boolean mResolvingError = false;
	private static final int REQUEST_RESOLVE_ERROR = 1001;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, arg2);
		if (requestCode == REQUEST_RESOLVE_ERROR) {
			// Toast.makeText(this, "resolved:"+resultCode,
			// Toast.LENGTH_SHORT).show();

			mResolvingError = false;
			if (resultCode == RESULT_OK) {

				// Make sure the app is not already connected or attempting to
				// connect
				if (!mGoogleApiClient.isConnecting()
						&& !mGoogleApiClient.isConnected()) {
					mGoogleApiClient.connect();
				}
			} else
				Toast.makeText(this, R.string.no_internet, Toast.LENGTH_LONG)
						.show();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (result.hasResolution()) {
			try {
				mResolvingError = true;

				result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
			} catch (SendIntentException e) {
				mGoogleApiClient.connect();
			}
		} else {
			achClicked = false;
			leaderClicked = false;
			mResolvingError = true;
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		updateGameServices();
	}

	public void updateGameServices() {
		if(!mGoogleApiClient.isConnected()){
			mGoogleApiClient.connect();
			return;
		}
		if (pref.contains("score")) {
			int val = pref.getInt("score", 0);
			Games.Leaderboards.submitScore(mGoogleApiClient,
					MyConstants.LEADERBOARD, val);

			if (pref.contains("zerodone")) {
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH0S);

			} else if (pref.contains("onedone")) {
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH1S);
			}
			if (val <= 50) {
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH350);
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH150);
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH50);

			} else if (val < 150) {
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH150);
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH350);

			} else if (val < 350)
				Games.Achievements.unlock(mGoogleApiClient, MyConstants.ACH350);

		}
		if (leaderClicked) {
			leaderClicked = false;
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(
					mGoogleApiClient, MyConstants.LEADERBOARD),
					REQUEST_LEADERBOARD);
		} else if (achClicked) {
			achClicked = false;
			startActivityForResult(
					Games.Achievements.getAchievementsIntent(mGoogleApiClient),
					REQUEST_ACHIEVEMENTS);
		}

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();

	}


    @Override
    public void onPlayClicked() {
		if(levelFragment == null){
			levelFragment = LevelFragment.newInstance();
		}
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.setCustomAnimations(R.anim.rotate_in_left, R.anim.rotate_out_top, R.anim.rotate_in_top,R.anim.rotate_out_left);
		transaction.replace(R.id.fragmentContainer,levelFragment).addToBackStack(TRANSACTION_MENU_TO_LEVEL).commit();

    }

	@Override
	public void onHelpClicked() {
		playFragment = PlayFragment.newInstance(4,EASY,1);
		FragmentTransaction transaction = fragmentManager.beginTransaction()
				.setCustomAnimations(R.anim.rotate_in_left, R.anim.rotate_out_top, R.anim.rotate_in_top, R.anim.rotate_out_left)
				.replace(R.id.fragmentContainer, playFragment).addToBackStack(TRANSACTION_LEVEL_TO_PLAY);
		transaction.commit();

	}


	@Override
	public void onSplashDone() {
		if(menuLayout == null){
			menuLayout = MenuLayout.newInstance();
		}
		fragmentManager.beginTransaction()
				.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
				.replace(R.id.fragmentContainer, menuLayout).commit();
	}

	@Override
	public void onLevelSelected(int level,boolean shouldAddToBackStack) {

		init();
		selectedLevel = level;

		switch (level){
			case LevelFragment.LEVEL_1:
				SIZE=4;
				MODE = EASY;
				break;
			case LevelFragment.LEVEL_2:
				SIZE=4;
				MODE = MEDIUM;
				break;
			case LevelFragment.LEVEL_3:
				SIZE=5;
				MODE = MEDIUM;
				break;
			case LevelFragment.LEVEL_4:
				SIZE=6;
				MODE = MEDIUM;
				break;
		}

		playFragment = PlayFragment.newInstance(SIZE,MODE,selectedLevel);
		FragmentTransaction transaction = fragmentManager.beginTransaction()
				.setCustomAnimations(R.anim.rotate_in_left, R.anim.rotate_out_top, R.anim.rotate_in_top, R.anim.rotate_out_left)
				.replace(R.id.fragmentContainer, playFragment).addToBackStack(TRANSACTION_LEVEL_TO_PLAY);
		transaction.commit();
		//fragmentContainer.startAnimation(slideOutLeftFragmentContainer);
		//gameContainer.startAnimation(slideInRightGameContainer);
		//gameStarted=true;


	}

	@Override
	public void callUpdateGameServices() {
		updateGameServices();
	}

	@Override
	public void onLevelCompleted(int level) {

		level++;
		if(level<=LevelFragment.LEVEL_4){
			onLevelSelected(level,false);
			pref.edit().putInt(MyConstants.LATEST_LEVEL,level).apply();
		}

	}

	@Override
	public void onLevelRefreshed(int level) {
		if(playFragment!=null){
			playFragment.refresh();
		}else{
			onLevelCompleted(level-1);
		}
	}

	@Override
	public void onCloseClicked() {
		fragmentManager.popBackStack(TRANSACTION_LEVEL_TO_PLAY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}
}
