package redpi.apps.binarygame;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import redpi.apps.utils.MyConstants;


public class LevelFragment extends Fragment implements View.OnClickListener {

    public static final int LEVEL_COUNT = 4;
    public  static final int LEVEL_1 = 1;
    public  static final int LEVEL_2 = 2;
    public  static final int LEVEL_3 = 3;
    public  static final int LEVEL_4 = 4;
    int height;
    int width;

    SharedPreferences pref;




    private OnFragmentInteractionListener mListener;



    private TextView level1Text;
    private TextView level2Text;
    private TextView level3Text;
    private TextView level4Text;
    private TextView currentSelected;
    private TextView gridValue;
    private TextView colorValue;
    private TextView levelType;
    private ImageView star1;
    private  ImageView star2;
    private  ImageView star3;
    private RelativeLayout levelContentContainer;
    private int selectedLevel = 1;
    private Animation scaleAnimation;
    private int stars=0;
    private  int highScore =0;
    private TextView tapToStart;
    private Animation alphaHalfAnimation;
    private TextView levelNumberText;
    private TextView bestText;

    public static LevelFragment newInstance() {
        LevelFragment fragment = new LevelFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public LevelFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                });
            }
        }

        return animation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_level, container, false);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), MyConstants.BELGRANO_REG);


        level1Text = (TextView) rootView.findViewById(R.id.levelOneText);
        level2Text = (TextView) rootView.findViewById(R.id.levelTwoText);
        level3Text = (TextView) rootView.findViewById(R.id.levelThreeText);
        level4Text = (TextView) rootView.findViewById(R.id.levelFourText);
        levelNumberText = (TextView) rootView.findViewById(R.id.levelNumberText);
        bestText = (TextView) rootView.findViewById(R.id.bestText);


        gridValue = (TextView) rootView.findViewById(R.id.gridValue);
        colorValue = (TextView) rootView.findViewById(R.id.colorsValue);
        levelType = (TextView) rootView.findViewById(R.id.levelTypeText);

        level1Text.setTypeface(tf);
        level2Text.setTypeface(tf);
        level3Text.setTypeface(tf);
        level4Text.setTypeface(tf);
        levelNumberText.setTypeface(tf);
        levelType.setTypeface(tf);
        gridValue.setTypeface(tf);
        colorValue.setTypeface(tf);
        bestText.setTypeface(tf);



        star1 = (ImageView)rootView.findViewById(R.id.star1);
        star2 = (ImageView)rootView.findViewById(R.id.star2);
        star3 = (ImageView)rootView.findViewById(R.id.star3);


        levelContentContainer = (RelativeLayout) rootView.findViewById(R.id.levelContentContainer);

        level1Text.setOnClickListener(this);
        level2Text.setOnClickListener(this);
        level3Text.setOnClickListener(this);
        level4Text.setOnClickListener(this);
        levelContentContainer.setOnClickListener(this);
        pref = getActivity().getSharedPreferences(MyConstants.PREF_NAME, Context.MODE_PRIVATE);

        width = pref.getInt(MyConstants.WIDTH,0);
        height = pref.getInt(MyConstants.HEIGHT,0);
        if(width ==0 || height==0) {
            Point size = MyConstants.getHeightWidth(getActivity());
            width = size.x;
            height=size.y;
            SharedPreferences.Editor edit = pref.edit();
            edit.putInt(MyConstants.WIDTH, width);
            edit.putInt(MyConstants.HEIGHT, height);
            edit.apply();
        }

        tapToStart = (TextView)rootView.findViewById(R.id.tap_to_start);
        scaleAnimation = AnimationUtils.loadAnimation(getActivity(),R.anim.level_scale_animation);
        alphaHalfAnimation = AnimationUtils.loadAnimation(getActivity(),R.anim.alpha_half_animation);
        tapToStart.startAnimation(alphaHalfAnimation);
        initLevelDimens();

        selectedLevel = pref.getInt(MyConstants.LATEST_LEVEL,1);
        switch (selectedLevel){
            case LEVEL_1:
                currentSelected = level1Text;
                stars = pref.getInt(MyConstants.LEVEL1_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL1_HS,0);
                break;
            case LEVEL_2:
                currentSelected = level2Text;
                stars = pref.getInt(MyConstants.LEVEL2_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL2_HS,0);
                break;
            case LEVEL_3:
                currentSelected = level3Text;
                stars = pref.getInt(MyConstants.LEVEL3_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL3_HS,0);
                break;
            case LEVEL_4:
                currentSelected = level4Text;
                stars = pref.getInt(MyConstants.LEVEL4_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL4_HS,0);
                break;
        }

        currentSelected.setBackgroundResource(R.drawable.level_selected_bg);
        setLevelContent(selectedLevel);
        animateLevel(currentSelected);
        return rootView;
    }

    private void setLevelContent(int selectedLevel) {

        levelNumberText.setText("LEVEL " + selectedLevel);
        if(highScore!=0){
            bestText.setText("BEST : " + highScore);
            bestText.setVisibility(View.VISIBLE);
        }else{
            bestText.setVisibility(View.GONE);
        }
        switch (selectedLevel){
            case LEVEL_1 :
                gridValue.setText("4 x 4");
                colorValue.setText("2 COLORS");
                levelType.setText("EASY");
                break;
            case LEVEL_2 :
                gridValue.setText("4 x 4");
                colorValue.setText("3 COLORS");
                levelType.setText("MEDIUM");
                break;
            case LEVEL_3 :
                gridValue.setText("5 x 5");
                colorValue.setText("3 COLORS");
                levelType.setText("HARD");
                break;
            case LEVEL_4 :
                gridValue.setText("6 x 6");
                colorValue.setText("3 COLORS");
                levelType.setText("INSANE");
                break;

        }
        setStars();

    }
    public void setStars(){
        switch (stars){
            case 3:
                star1.setBackgroundResource(R.drawable.star_activated);
                star2.setBackgroundResource(R.drawable.star_activated);
                star3.setBackgroundResource(R.drawable.star_activated);
                break;
            case 2:
                star1.setBackgroundResource(R.drawable.star_activated);
                star2.setBackgroundResource(R.drawable.star_activated);
                star3.setBackgroundResource(R.drawable.star_deactivated);
                break;
            case 1:
                star1.setBackgroundResource(R.drawable.star_activated);
                star2.setBackgroundResource(R.drawable.star_deactivated);
                star3.setBackgroundResource(R.drawable.star_deactivated);
                break;
            case 0:
                star1.setBackgroundResource(R.drawable.star_deactivated);
                star2.setBackgroundResource(R.drawable.star_deactivated);
                star3.setBackgroundResource(R.drawable.star_deactivated);

        }
    }

    private void initLevelDimens() {


        int levelMargin = (int) getResources().getDimension(R.dimen.levelTextMargin);
       int totalMargin = (int) ((LEVEL_COUNT + 1) *levelMargin );
        int levelWidth = (width - totalMargin)/LEVEL_COUNT;

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) level1Text.getLayoutParams();
        params.width = levelWidth;
        params.height = levelWidth;
        //level1Text.setTextSize(10);

        params = (LinearLayout.LayoutParams) level2Text.getLayoutParams();
        params.width = levelWidth;
        params.height = levelWidth;

        params = (LinearLayout.LayoutParams) level3Text.getLayoutParams();
        params.width = levelWidth;
        params.height = levelWidth;

        params = (LinearLayout.LayoutParams) level4Text.getLayoutParams();
        params.width = levelWidth;
        params.height = levelWidth;

        int contentHeight = height - levelWidth - 4*levelMargin;
        RelativeLayout.LayoutParams paramsContent = (RelativeLayout.LayoutParams) levelContentContainer.getLayoutParams();
        paramsContent.height = contentHeight;

        int starSize = (int) (width - 2*getResources().getDimension(R.dimen.levelTextMargin) -
                4*getResources().getDimension(R.dimen.level_star_margin))/6;

        star1.getLayoutParams().height = starSize;
        star1.getLayoutParams().width = starSize;
        star2.getLayoutParams().height = starSize;
        star2.getLayoutParams().width = starSize;
        star3.getLayoutParams().height =starSize;
        star3.getLayoutParams().width =starSize;



    }

    private void animateLevel(TextView selectedTextView) {

        currentSelected.clearAnimation();
        currentSelected.setBackgroundResource(R.drawable.level_bg);
        currentSelected = selectedTextView;
        currentSelected.setBackgroundResource(R.drawable.level_selected_bg);
        currentSelected.startAnimation(scaleAnimation);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.levelOneText:
                selectedLevel = 1;
                stars = pref.getInt(MyConstants.LEVEL1_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL1_HS,0);
                animateLevel(level1Text);
                setLevelContent(selectedLevel);
                break;
            case R.id.levelTwoText:
                selectedLevel = 2;
                stars = pref.getInt(MyConstants.LEVEL2_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL2_HS,0);
                animateLevel(level2Text);
                setLevelContent(selectedLevel);
                break;
            case R.id.levelThreeText:
                selectedLevel = 3;
                stars = pref.getInt(MyConstants.LEVEL3_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL3_HS,0);
                animateLevel(level3Text);
                setLevelContent(selectedLevel);
                break;
            case R.id.levelFourText:
                selectedLevel = 4;
                stars = pref.getInt(MyConstants.LEVEL4_STARS,0);
                highScore = pref.getInt(MyConstants.LEVEL4_HS,0);
                animateLevel(level4Text);
                setLevelContent(selectedLevel);
                break;
            case R.id.levelContentContainer:
                mListener.onLevelSelected(selectedLevel,true);
                break;
        }

    }


    public interface OnFragmentInteractionListener {
        public void onLevelSelected(int level,boolean  shouldAddToBackstack);
    }

}
