package redpi.apps.binarygame;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import redpi.apps.utils.MyConstants;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenuLayout.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuLayout#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuLayout extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int SIZE = 4 ;
    View rootView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private TextView[] tileArray;
    private int totalCount=0;
    private Timer timer;
    private ImageView play;
    private ImageView rate;
    private ImageView help;
    private int width;
    private int height;
    private Animation rotationAnimation;

        public static MenuLayout newInstance() {
        MenuLayout fragment = new MenuLayout();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public MenuLayout() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_menu_layout, container, false);
        SharedPreferences pref = getActivity().getSharedPreferences(MyConstants.PREF_NAME, Context.MODE_PRIVATE);
        width =pref.getInt(MyConstants.WIDTH,0);
        height = pref.getInt(MyConstants.HEIGHT,0);

        if(width ==0 || height==0) {
            Point size = MyConstants.getHeightWidth(getActivity());
            width = size.x;
            height=size.y;
            SharedPreferences.Editor edit = pref.edit();
            edit.putInt(MyConstants.WIDTH, width);
            edit.putInt(MyConstants.HEIGHT, height);
            edit.apply();
        }

        Typeface tf = Typeface.createFromAsset(getResources().getAssets(),"fonts/generica.otf");
        TextView turn = (TextView) rootView.findViewById(R.id.turn);
        TextView it = (TextView) rootView.findViewById(R.id.the);
        TextView tiles = (TextView) rootView.findViewById(R.id.tiles);
        play = (ImageView)rootView.findViewById(R.id.play);
        play.setOnClickListener(this);
        turn.setTypeface(tf);
        it.setTypeface(tf);
        tiles.setTypeface(tf);
        populateManual(SIZE);
        startAnimation();
        rotationAnimation=AnimationUtils.loadAnimation(getActivity(),R.anim.rotation);
        rotationAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                try {
                    timer.cancel();
                }catch (Exception e){

                }
                mListener.onPlayClicked();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        rate = (ImageView)rootView.findViewById(R.id.rate);
        help = (ImageView)rootView.findViewById(R.id.help);

        rate.setOnClickListener(this);
        help.setOnClickListener(this);
        return rootView;
    }

    private class AnimateTask extends TimerTask{
        final Random random = new Random();

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    animateHandler(random.nextInt(SIZE*SIZE));
                }
            });
        }
    }

    public void startAnimation()
    {



        //timer.scheduleAtFixedRate(animateTask,1000,1500);
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            timer = new Timer();
            TimerTask animateTask = new AnimateTask();
            timer.scheduleAtFixedRate(animateTask,1000,1500);
        }finally {

        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                });
            }
        }

        return animation;
    }

    public void populateManual(int size)
    {
        LinearLayout tileContainer = (LinearLayout) rootView.findViewById(R.id.tileContainer);
        int marginCount = size*2 + 2;
        tileArray = new TextView[size*size];
        int margin = (int)getResources().getDimension(R.dimen.margin_tile);
        int tileWidth = (int)(width/2 - marginCount*margin)/size;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(tileWidth,tileWidth);
        int fontSize = (int)getResources().getDimension(R.dimen.font_size);
        params.setMargins(margin,margin,margin,margin);


        int fillCount=0;
        boolean one = true;
        for(int i = 0;i<size;++i)
        {
            LinearLayout row = new LinearLayout(getActivity());
            for(int j=0;j<size;++j)
            {
                TextView textView = new TextView(getActivity());
                textView.setLayoutParams(params);
                if(one){
                textView.setBackgroundColor(MyConstants.OneColor);
                    textView.setTag(1);

                }else{
                    textView.setBackgroundColor(MyConstants.zeroColor);
                    textView.setTag(0);
                }
                one = !one;
                row.addView(textView);
                totalCount++;
                tileArray[fillCount] = textView;
                fillCount++;
            }
            tileContainer.addView(row);
        }


        //setting playbutton size
        float coveredTop = (4*tileWidth + margin*marginCount)
                + getResources().getDimension(R.dimen.margin_top_title)
                + getResources().getDimension(R.dimen.margin_bottom_title)
                + getResources().getDimension(R.dimen.title_font_size_big);
        float coveredBottom = getResources().getDimension(R.dimen.star_size)
                +getResources().getDimension(R.dimen.margin_help_bottom);
        float playButtonMargin = getResources().getDimension(R.dimen.playButtonMargin);
        int verticalUsableArea = (int)(height - coveredTop - coveredBottom - 2*playButtonMargin);
        int horizontalUsableArea = (int) (width - 2*playButtonMargin);
        int playButtonSize = horizontalUsableArea<verticalUsableArea ? horizontalUsableArea : verticalUsableArea;
        play.getLayoutParams().height = playButtonSize;
        play.getLayoutParams().width = playButtonSize;


    }


    public void animateHandler(int pos)
    {
        int tile1Pos = pos;

        int tile2Pos = tile1Pos + SIZE;
        int tile3Pos = tile1Pos - SIZE;
        int tile4Pos = tile1Pos - 1;
        int tile5Pos = tile1Pos + 1;

        animateFunc(tileArray[pos]);
        if (tile2Pos < 16)
            animateFunc(tileArray[tile2Pos]);
        if (tile3Pos > -1) {
            animateFunc(tileArray[tile3Pos]);

        }

        if (tile1Pos%SIZE !=0)
            animateFunc(tileArray[tile4Pos]);
        if ((tile1Pos+1)%SIZE != 0)
            animateFunc(tileArray[tile5Pos]);

    }
    public void animateFunc(final View view) {


        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "RotationY", 0,
                90);
        animator.setDuration(400);
        animator.setRepeatCount(1);
        animator.setRepeatMode(Animation.REVERSE);
        animator.addListener(new Animator.AnimatorListener() {

            TextView tempView = (TextView) view;
            int textVal;

            @Override
            public void onAnimationStart(Animator arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animator arg0) {
                // TODO Auto-generated method stub
                //textVal = Integer.parseInt(tempView.getText().toString());
                textVal = (Integer)tempView.getTag();
                if (textVal == 0) {
                    totalCount += 1;
                    tempView.setTag(1);
                    tempView.setBackgroundColor(MyConstants.OneColor);

                } else if (textVal == 1) {
                        totalCount -= 1;
                    tempView.setTag(0);
                        tempView.setBackgroundColor(MyConstants.zeroColor);
                    }


                }




            @Override
            public void onAnimationEnd(Animator arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationCancel(Animator arg0) {
                // TODO Auto-generated method stub

            }
        });
        animator.start();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            timer.cancel();
        }catch (Exception e){

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener=null;

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.play:
                v.startAnimation(rotationAnimation);
                break;
            case R.id.rate:
                Uri uri = Uri.parse("market://details?id="
                        + "redpi.apps.binarygame");
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=redpi.apps.binarygame")));
                }
                break;
            case R.id.help:
                getActivity().getSharedPreferences(MyConstants.PREF_NAME,Context.MODE_PRIVATE)
                        .edit().putBoolean(MyConstants.SHOW_HELP,true).apply();
                mListener.onHelpClicked();
                break;

        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onPlayClicked();
        public void onHelpClicked();
    }

}
