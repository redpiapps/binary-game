package redpi.apps.binarygame;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import redpi.apps.utils.MyConstants;


public class LevelCompleteDialog extends DialogFragment implements View.OnClickListener {

    private static final String LEVEL = "level";
    private static final String STEPS = "steps";


    private int currentLevel=1;

    private int steps = 0;

    private OnFragmentInteractionListener mListener;
    private View rootView;
    private TextView stepsView;
    private TextView bestView;
    private TextView stepsTitle;
    private TextView bestTitle;
    private ImageView star1;
    private ImageView star2;
    private ImageView star3;
    private int stars;
    private TextView movesTitle;
    private LinearLayout restart;
    private LinearLayout next;
    private LinearLayout close;
    private TextView successText;




    public static LevelCompleteDialog newInstance(int currentLevel,int steps) {
        LevelCompleteDialog fragment = new LevelCompleteDialog();
        Bundle args = new Bundle();
        args.putInt(LEVEL, currentLevel);
        args.putInt(STEPS,steps);
        fragment.setArguments(args);
        return fragment;
    }

    public LevelCompleteDialog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentLevel = getArguments().getInt(LEVEL);
            steps = getArguments().getInt(STEPS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), MyConstants.BELGRANO_REG);
        rootView = inflater.inflate(R.layout.fragment_level_complete_dialog, container, false);
        stepsView = (TextView) rootView.findViewById(R.id.stepsView);
        bestView = (TextView)rootView.findViewById(R.id.bestView);
       // stepsTitle = (TextView) rootView.findViewById(R.id.stepsTitle);
        bestTitle = (TextView)rootView.findViewById(R.id.bestTitle);
        movesTitle = (TextView)rootView.findViewById(R.id.movesTitle);
        restart = (LinearLayout)rootView.findViewById(R.id.refresh);
        next = (LinearLayout)rootView.findViewById(R.id.next);
        close = (LinearLayout)rootView.findViewById(R.id.close);
        successText = (TextView)rootView.findViewById(R.id.successText);
        restart.setOnClickListener(this);
        next.setOnClickListener(this);
        close.setOnClickListener(this);

        stepsView.setTypeface(tf);
        bestView.setTypeface(tf);
        bestTitle.setTypeface(tf);
        movesTitle.setTypeface(tf);
        successText.setTypeface(tf);
        if(currentLevel == LevelFragment.LEVEL_4){
            successText.setText("THE END");
            restart.setVisibility(View.GONE);
            next.setVisibility(View.GONE);
            close.setVisibility(View.VISIBLE);
        }
        star1 = (ImageView)rootView.findViewById(R.id.star1);
        star2 = (ImageView)rootView.findViewById(R.id.star2);
        star3 = (ImageView)rootView.findViewById(R.id.star3);
        stepsView.setText(""+steps);

        setStars();
        setPrefs();



        return rootView;
    }

    public void setPrefs(){
        SharedPreferences pref = getActivity().getSharedPreferences(MyConstants.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        String starsPref = "";
        String highScorePref = "";
        switch (currentLevel){
            case LevelFragment.LEVEL_1:
                highScorePref = MyConstants.LEVEL1_HS;
                starsPref = MyConstants.LEVEL1_STARS;
                break;
            case LevelFragment.LEVEL_2:
                highScorePref = MyConstants.LEVEL2_HS;
                starsPref = MyConstants.LEVEL2_STARS;
                break;
            case LevelFragment.LEVEL_3:
                highScorePref = MyConstants.LEVEL3_HS;
                starsPref = MyConstants.LEVEL3_STARS;
                break;
            case LevelFragment.LEVEL_4:
                highScorePref = MyConstants.LEVEL4_HS;
                starsPref = MyConstants.LEVEL4_STARS;
                break;

        }
        int prevScore = pref.getInt(highScorePref, 0);
        int prevStars = pref.getInt(starsPref,0);


        if ((prevScore == 0) || (steps<prevScore)) {
            edit.putInt(highScorePref, steps);
            edit.apply();
            bestView.setText(String.valueOf(steps));
        }else{
            bestView.setText(String.valueOf(pref.getInt(highScorePref,steps)));
        }

        if ((prevStars == 0) || (stars>prevStars)) {
            edit.putInt(starsPref, stars);
            edit.apply();
        }

    }

    public void setStars(){
        switch(currentLevel){
            case LevelFragment.LEVEL_1:
                if(steps<=6){
                    star2.setImageResource(R.drawable.star_activated);
                    star3.setImageResource(R.drawable.star_activated);
                    stars = 3;
                }else if(steps<=12){
                    star2.setImageResource(R.drawable.star_activated);
                    stars = 2;
                }else{
                    stars =1 ;
                }
                break;
            case LevelFragment.LEVEL_2:
            case LevelFragment.LEVEL_3:
                if(steps <= 26 ){
                    star2.setImageResource(R.drawable.star_activated);
                    star3.setImageResource(R.drawable.star_activated);
                    stars= 3;
                }else if(steps<=50){
                    star2.setImageResource(R.drawable.star_activated);
                    stars=2;
                }else{
                    stars=1;
                }
                break;

            case LevelFragment.LEVEL_4:
                if(steps <= 34 ){
                    star2.setImageResource(R.drawable.star_activated);
                    star3.setImageResource(R.drawable.star_activated);
                    stars=3;
                }else if(steps<= 60){
                    star2.setImageResource(R.drawable.star_activated);
                    stars=2;

                }else{
                    stars=1;
                }
                break;
        }
    }
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                });
            }
        }

        return animation;
    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationTheme;
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.gameover_dialog_bg);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
           throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.refresh:
                mListener.onLevelRefreshed(currentLevel-1);
                dismiss();
                break;
            case R.id.next:
                dismiss();
                mListener.onLevelCompleted(currentLevel);
                break;
            case R.id.close:
                dismiss();
                getActivity().getSharedPreferences(MyConstants.PREF_NAME,Context.MODE_PRIVATE).edit().putInt(MyConstants.LATEST_LEVEL,currentLevel).apply();
                mListener.onCloseClicked();
                break;
        }
    }


    public interface OnFragmentInteractionListener {
        public void onLevelCompleted(int level);
        public void onLevelRefreshed(int level);
        public void onCloseClicked();
    }


}
