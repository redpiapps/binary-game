package redpi.apps.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.view.Display;


public class MyConstants {
	public  static final int OneColor = Color.parseColor("#F78181");
	public  static final int zeroColor = Color.parseColor("#81DAF5");
    public  static final int twoColor = Color.parseColor("#ad08cf07");

    public static final int padding_dp = 5;
	public static int MARGIN_COUNT=10;
	public static final String FONT_CCAP = "fonts/ccaps.ttf";
	public static final String FONT_QUATR = "fonts/quattr.ttf";
	public static final String BELGRANO_REG = "fonts/belgrano_regular.ttf";


	public static final String FONT_EPYVAL = "fonts/epyval.ttf";
	public static final String GAME_URL = "http://play.google.com/store/apps/details?id=redpi.apps.binarygame";
	public static final String LEADERBOARD = "CgkIhfPfhIcQEAIQBg";

	public static final String ACH1S = "CgkIhfPfhIcQEAIQAQ";
	public static final String ACH0S = "CgkIhfPfhIcQEAIQAg";

	public static final String ACH350 = "CgkIhfPfhIcQEAIQBQ";
	public static final String ACH150 = "CgkIhfPfhIcQEAIQBA";
	public static final String ACH50 = "CgkIhfPfhIcQEAIQAw";

	public static final int NORMAL_DURATION=150;

	public static final String INTER_AD_ID = "ca-app-pub-9801018413360964/5512707537";

	public static final String LEVEL1_HS = "level1HighScore";
	public static final String LEVEL2_HS = "level2HighScore";
	public static final String LEVEL3_HS = "level3HighScore";
	public static final String LEVEL4_HS = "level4HighScore";

	public static final String LEVEL1_STARS = "level1Stars";
	public static final String LEVEL2_STARS = "level2Stars";
	public static final String LEVEL3_STARS = "level3Stars";
	public static final String LEVEL4_STARS = "level4Stars";

	public static final String PREF_NAME = "myPref";

	public static final String HEIGHT = "height";
	public static final String WIDTH = "width";

	public static final String LATEST_LEVEL = "latestLevel";
	public static final String SHOW_HELP = "showHelp";

	public static Point getHeightWidth(Activity activity){
		Display display = activity.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		return size;
	}



	
}
